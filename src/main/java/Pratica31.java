
/**
 * @author Lucio Agostinho Rocha
 * @since 26/09/2016
 */
 

import java.util.Date;
import java.util.GregorianCalendar;

public class Pratica31 {
    //(Item8)
    private static Date inicio = new Date();    
    
    //(Item4)
    private static String meuNome = new String("lUCio AGOstinhO roChA");
    
    public static void main(String [] args){            
    
        meuNome = meuNome.toLowerCase(); //Converte para minusculo
    
        //(Item5)
        System.out.println(meuNome.toUpperCase()); //LUCIO AGOSTINHO ROCHA
        
        //(Item6)
        String inicial1 = meuNome.substring(0,1).toUpperCase(); //L.
        String inicial2 = meuNome.substring(6,7).toUpperCase(); //A.
        System.out.println(
                meuNome.substring(16,17).toUpperCase() + //R
                meuNome.substring(17,meuNome.length()) + //ocha
                ", " + 
                inicial1 + ". " + //L.
                inicial2 + ". "); //A.
        
        //(Item7)
        GregorianCalendar dataNascimento = new GregorianCalendar(1982, GregorianCalendar.APRIL, 29);
        GregorianCalendar hoje = new GregorianCalendar(2014, GregorianCalendar.SEPTEMBER, 26);
        
        long tempoDecorrido = hoje.getTime().getTime() - dataNascimento.getTime().getTime();
        tempoDecorrido = tempoDecorrido /1000; //tempo decorrido em segundos        
        tempoDecorrido = tempoDecorrido / 60; //" " minutos
        tempoDecorrido = tempoDecorrido / 60; //" " horas
        tempoDecorrido = tempoDecorrido / 24; // " " dias                
        //tempoDecorrido = tempoDecorrido / 365; // " " anos                
        
        System.out.println((int) tempoDecorrido+ " dias");
        
        //(Item8)
        Date fim = new Date();        
        System.out.println("Tempo decorrido: " + Math.abs(inicio.getTime()-fim.getTime()) + " ms");
    
    }//fim main
    
}//fim classe
